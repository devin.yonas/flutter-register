import 'package:flutter/material.dart';

const Color primaryBgColor = Colors.blueAccent;

Map<int, String> weekDayString = {
  1: 'Senin',
  2: 'Selasa',
  3: 'Rabu',
  4: 'Kamis',
  5: 'Jumat',
  6: 'Sabtu',
  7: 'Minggu',
};

Map<int, String> monthString = {
  1: 'Jan',
  2: 'Feb',
  3: 'Mar',
  4: 'Apr',
  5: 'Mei',
  6: 'Jun',
  7: 'Jul',
  8: 'Agt',
  9: 'Sep',
  10: 'Okt',
  11: 'Nov',
  12: 'Des',
};

///Date Formatter
String datetimeToString(DateTime date) {
  final weekday = weekDayString[date.weekday];
  final day = date.day.toString().padLeft(2, '0');
  final month = monthString[date.month];
  final year = date.year;

  return '$weekday, $day $month $year';
}
