import 'package:flutter/material.dart';

class PulsingIcon extends StatefulWidget {
  final Widget icon;
  PulsingIcon({required this.icon});

  @override
  _PulsingIconState createState() => _PulsingIconState();
}

class _PulsingIconState extends State<PulsingIcon> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _animation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1500));
    _animationController.repeat(reverse: true);
    _animation = Tween(begin: 0.0, end: 20.0).animate(_animationController)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    super.dispose();
    _animation.removeListener(() {});
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(22),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            color: Colors.white24,
            blurRadius: 0,
            spreadRadius: _animation.value,
          ),
        ],
      ),
      child: widget.icon,
    );
  }
}
