import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {
  final int progressNum;
  ProgressBar({required this.progressNum});

  @override
  Widget build(BuildContext context) {
    List<Widget> progressBars = List.generate(4, (index) => circleNumber(index + 1, status: index < progressNum));

    return Material(
      color: Colors.transparent,
      child: Container(
        margin: EdgeInsets.all(20),
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Container(
              width: double.infinity,
              height: 4,
              color: Colors.black,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: progressBars,
            ),
          ],
        ),
      ),
    );
  }

  Widget circleNumber(int num, {required bool status}) {
    return Center(
      child: Container(
        width: 60.0,
        height: 60.0,
        decoration: new BoxDecoration(
          color: status ? Colors.green : Colors.white,
          shape: BoxShape.circle,
          border: Border.all(),
        ),
        child: Center(
            child: Text(
          '$num',
          style: TextStyle(fontSize: 25),
        )),
      ),
    );
  }
}
