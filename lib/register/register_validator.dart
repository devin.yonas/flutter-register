final RegExp _email = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
final RegExp _numeric = RegExp('[0-9]');
final RegExp _upperCase = RegExp('[A-Z]');
final RegExp _lowerCase = RegExp('[a-z]');

enum PasswordStrength { weak, medium, strong }

extension ParseToString on Object {
  String enumToString() {
    return this.toString().split('.').last.toUpperCase();
  }
}

bool isEmail(String? input) {
  if (input == null || input.isEmpty) {
    return false;
  }
  return _email.hasMatch(input);
}

bool containNumeric(String input) => input.contains(_numeric);
bool containUpperCase(String input) => input.contains(_upperCase);
bool containLowerCase(String input) => input.contains(_lowerCase);
