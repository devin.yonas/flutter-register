import 'package:flutter/material.dart';
import 'package:jago_test/register/register_controller.dart';
import 'package:jago_test/register/register_screen1.dart';

class RegisterPage extends StatelessWidget {
  final bloc = RegisterPageController();

  @override
  Widget build(BuildContext context) {
    return RegisterScreen1(bloc: bloc);
  }
}
