import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jago_test/globals/globals.dart';
import 'package:jago_test/register/register_controller.dart';
import 'package:jago_test/register/register_end.dart';
import 'package:jago_test/widgets/progress_bar.dart';
import 'package:jago_test/widgets/pulsing_icon.dart';

class RegisterScreen4 extends StatefulWidget {
  final RegisterPageController bloc;
  RegisterScreen4({required this.bloc});

  @override
  _RegisterScreen4State createState() => _RegisterScreen4State();
}

class _RegisterScreen4State extends State<RegisterScreen4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Create Account',
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: primaryBgColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Hero(
              tag: 'ProgressBar',
              child: ProgressBar(progressNum: 3),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: PulsingIcon(icon: Icon(Icons.event, size: 40, color: Colors.blue)),
            ),
            Flexible(child: _content()),
            _nextButton(context),
          ],
        ),
      ),
    );
  }

  Widget _content() {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(
                'Schedule Video Call',
                style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.w700),
              )),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: Text(
              'Choose the date and time that you preferred, we will send a link via email to make a video call on the scheduled date and time.',
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 17, color: Colors.white),
            ),
          ),
          SizedBox(height: 10),
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    margin: EdgeInsets.only(top: 15, bottom: 15),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: ListTile(
                      title: Text('Date'),
                      subtitle: Text(widget.bloc.chosenDate),
                      trailing: Icon(Icons.arrow_drop_down),
                      onTap: () async {
                        final date = Platform.isAndroid
                            ? await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate: DateTime.now().add(Duration(days: 30)),
                              )
                            : await iOSDateTimePicker(mode: CupertinoDatePickerMode.date);

                        if (date != null) widget.bloc.chosenDate = datetimeToString(date);
                        setState(() {});
                      },
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    margin: EdgeInsets.only(top: 15, bottom: 15),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: ListTile(
                      title: Text('Time'),
                      subtitle: Text(widget.bloc.chosenTime),
                      trailing: Icon(Icons.arrow_drop_down),
                      onTap: () async {
                        final time = Platform.isAndroid
                            ? await showTimePicker(context: context, initialTime: TimeOfDay.now())
                            : TimeOfDay.fromDateTime(await iOSDateTimePicker(mode: CupertinoDatePickerMode.time));

                        if (time != null) widget.bloc.chosenTime = '${time.hour}:${time.minute}';
                        setState(() {});
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _nextButton(BuildContext ctx) {
    return Container(
      height: 70,
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      color: Colors.black12,
      child: ElevatedButton(
        onPressed: widget.bloc.checkDateTimeChosen()
            ? () {
                Navigator.push(
                  ctx,
                  MaterialPageRoute(builder: (ctx) => RegisterEnd()),
                );
              }
            : null,
        child: Text('Next'),
      ),
    );
  }

  Future<DateTime> iOSDateTimePicker({required CupertinoDatePickerMode mode}) async {
    DateTime result = DateTime.now();
    await showCupertinoModalPopup(
        barrierDismissible: true,
        context: context,
        builder: (ctx) {
          return SafeArea(
            child: Container(
                height: 400,
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(child: CupertinoDatePicker(use24hFormat: true, mode: mode, onDateTimeChanged: (date) => result = date)),
                    CupertinoButton(child: Text('OK'), onPressed: () => Navigator.pop(ctx, result)),
                  ],
                )),
          );
        });

    return result;
  }
}
