import 'package:flutter/material.dart';
import 'package:jago_test/globals/globals.dart';
import 'package:jago_test/register/register_controller.dart';
import 'package:jago_test/register/register_screen4.dart';
import 'package:jago_test/widgets/progress_bar.dart';

class RegisterScreen3 extends StatefulWidget {
  final RegisterPageController bloc;
  RegisterScreen3({required this.bloc});

  @override
  _RegisterScreen3State createState() => _RegisterScreen3State();
}

class _RegisterScreen3State extends State<RegisterScreen3> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Create Account',
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: primaryBgColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Hero(
              tag: 'ProgressBar',
              child: ProgressBar(progressNum: 2),
            ),
            Flexible(child: _content()),
            _nextButton(context),
          ],
        ),
      ),
    );
  }

  Widget _content() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Text(
                'Personal Information',
                style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.w700),
              )),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: Text(
              'Please fill in the information belom and your goal for digital saving.',
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 17, color: Colors.white),
            ),
          ),
          SizedBox(height: 10),
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _dropDownOption(
                      title: 'Goal for activation',
                      options: widget.bloc.chooseGoals,
                      onChange: (val) {
                        widget.bloc.chosenGoals = val ?? '';
                        setState(() {});
                      }),
                  _dropDownOption(
                      title: 'Monthly Income',
                      options: widget.bloc.chooseMoneyRange,
                      onChange: (val) {
                        widget.bloc.chosenIncome = val ?? '';
                        setState(() {});
                      }),
                  _dropDownOption(
                      title: 'Monthly Expense',
                      options: widget.bloc.chooseMoneyRange,
                      onChange: (val) {
                        widget.bloc.chosenExpense = val ?? '';
                        setState(() {});
                      }),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _dropDownOption({required String title, required List<dynamic> options, required Function(String?)? onChange}) {
    final ddlItems = options
        .map((e) => DropdownMenuItem<String>(
              value: e.toString(),
              child: Text('$e'),
            ))
        .toList();

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      margin: EdgeInsets.only(top: 15, bottom: 15),
      padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('$title', style: TextStyle(color: Colors.grey)),
          DropdownButtonFormField(
            items: ddlItems,
            decoration: InputDecoration(border: InputBorder.none, hintText: '- Choose Option -'),
            onChanged: onChange,
          ),
        ],
      ),
    );
  }

  Widget _nextButton(BuildContext ctx) {
    return Container(
      height: 70,
      color: Colors.black12,
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: ElevatedButton(
        onPressed: widget.bloc.checkInformationChosen()
            ? () {
                Navigator.push(
                  ctx,
                  MaterialPageRoute(
                      builder: (ctx) => RegisterScreen4(
                            bloc: widget.bloc,
                          )),
                );
              }
            : null,
        child: Text('Next'),
      ),
    );
  }
}
