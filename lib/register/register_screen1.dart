import 'package:flutter/material.dart';
import 'package:jago_test/globals/globals.dart';
import 'package:jago_test/register/register_controller.dart';
import 'package:jago_test/register/register_screen2.dart';
import 'package:jago_test/register/register_validator.dart';
import 'package:jago_test/widgets/progress_bar.dart';
import 'dart:math' as math;

class RegisterScreen1 extends StatefulWidget {
  final RegisterPageController bloc;
  RegisterScreen1({required this.bloc});

  @override
  _RegisterScreen1State createState() => _RegisterScreen1State();
}

class _RegisterScreen1State extends State<RegisterScreen1> {
  @override
  void initState() {
    widget.bloc.tecEmail.addListener(() => setState(() {}));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    widget.bloc.tecEmail.removeListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryBgColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Flexible(
              child: Container(
                color: Colors.grey.shade100,
                child: ListView(
                  children: [
                    AppBar(
                      elevation: 0,
                      backgroundColor: primaryBgColor,
                    ),
                    Container(
                      color: primaryBgColor,
                      child: Hero(
                        tag: 'ProgressBar',
                        child: ProgressBar(progressNum: 0),
                      ),
                    ),
                    Container(
                      color: primaryBgColor,
                      height: 100,
                      child: CustomPaint(painter: ShapePainter()),
                    ),
                    _content(),
                  ],
                ),
              ),
            ),
            _nextButton(context),
          ],
        ),
      ),
    );
  }

  Widget _content() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(style: BorderStyle.none, width: 0, color: Colors.grey.shade100),
      ),
      padding: EdgeInsets.all(20).copyWith(top: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              text: 'Welcome to \n',
              style: TextStyle(fontSize: 40, color: Colors.black, fontWeight: FontWeight.w800),
              children: [
                TextSpan(text: 'GIN '),
                TextSpan(text: 'Finans', style: TextStyle(color: Colors.blue)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30, bottom: 30),
            child: Text(
              'Welcome to The Bank of The Future. Manage and Track your accounts on the go.',
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            margin: EdgeInsets.only(top: 30),
            padding: EdgeInsets.all(10),
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: TextFormField(
                  controller: widget.bloc.tecEmail,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if (!isEmail(value)) {
                      return 'Invalid Email Format';
                    }
                  },
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email_outlined),
                    hintText: 'Email',
                    border: InputBorder.none,
                  )),
            ),
          )
        ],
      ),
    );
  }

  Widget _nextButton(BuildContext ctx) {
    return Container(
      color: Colors.white,
      height: 70,
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: ElevatedButton(
        onPressed: isEmail(widget.bloc.tecEmail.text)
            ? () {
                Navigator.push(
                  ctx,
                  MaterialPageRoute(
                      builder: (ctx) => RegisterScreen2(
                            bloc: widget.bloc,
                          )),
                );
              }
            : null,
        child: Text('Next'),
      ),
    );
  }
}

class ShapePainter extends CustomPainter {
  final double divider = 7;

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.grey.shade100
      ..strokeWidth = 0
      ..style = PaintingStyle.fill
      ..strokeCap = StrokeCap.round;

    final curvePoint = size.width / divider;

    final curvePath = Path()
      ..moveTo(0, size.height + 10) //4
      ..quadraticBezierTo(0, 0, curvePoint + 10, 10) //8
      ..lineTo(size.width, size.height) //7
      ..close(); //9

    canvas.drawPath(curvePath, paint);

    // canvas.drawCircle(
    //   Offset(size.width / divider + curveRadius, curveRadius),
    //   curveRadius,
    //   paint,
    // );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
