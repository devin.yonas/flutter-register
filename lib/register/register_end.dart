import 'package:flutter/material.dart';
import 'package:jago_test/globals/globals.dart';
import 'package:jago_test/register/register_page.dart';
import 'package:jago_test/widgets/progress_bar.dart';
import 'package:jago_test/widgets/pulsing_icon.dart';

class RegisterEnd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: primaryBgColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Hero(
              tag: 'ProgressBar',
              child: ProgressBar(progressNum: 4),
            ),
            Flexible(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    PulsingIcon(icon: Icon(Icons.check_circle, size: 70, color: Colors.green)),
                    SizedBox(height: 10),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Registration Complete! \nPlease check your registered email.',
                        style: TextStyle(fontSize: 22),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            _doneButton(context),
          ],
        ),
      ),
    );
  }

  Widget _doneButton(BuildContext ctx) {
    return Container(
      height: 70,
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      color: Colors.black12,
      child: ElevatedButton(
        onPressed: () {
          Navigator.popUntil(ctx, (route) => route.isFirst);
          Navigator.pushReplacement(ctx, MaterialPageRoute(builder: (ctx) => RegisterPage()));
          // Navigator.popUntil(
          //   ctx,
          //   MaterialPageRoute(builder: (ctx) => RegisterScreen1()),
          // );
        },
        child: Text('Register Again'),
      ),
    );
  }
}
