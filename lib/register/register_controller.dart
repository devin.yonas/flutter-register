import 'package:jago_test/register/register_validator.dart';
import 'package:flutter/material.dart';

class RegisterPageController {
  RegisterPageController();

  bool showPassword = false;
  bool isLowerCase = false;
  bool isUpperCase = false;
  bool isNumeric = false;
  bool is9Chars = false;

  TextEditingController tecEmail = TextEditingController();
  TextEditingController tecPassword = TextEditingController();

  String complexityMeter = '';
  Color complexityMeterColor = Colors.white;

  final List chooseGoals = ['Saving', 'Investment', 'Payment'];
  final List chooseMoneyRange = ['< 5 million', '5 million - 10 million', '10 million - 20 million', '> 20 million'];

  String chosenGoals = '';
  String chosenIncome = '';
  String chosenExpense = '';

  String chosenDate = '- Choose Date -';
  String chosenTime = '- Choose Time -';

  bool checkDateTimeChosen() {
    if (chosenDate.isNotEmpty && chosenDate != '- Choose Date -' && chosenTime.isNotEmpty && chosenTime != '- Choose Time -') return true;
    return false;
  }

  bool checkInformationChosen() => (chosenGoals.isNotEmpty && chosenIncome.isNotEmpty && chosenExpense.isNotEmpty);

  void toggleShowPassword() => showPassword = !showPassword;

  void checkPassword(String password) {
    if (password.length > 9)
      is9Chars = true;
    else
      is9Chars = false;

    isNumeric = containNumeric(password);
    isUpperCase = containUpperCase(password);
    isLowerCase = containLowerCase(password);
    checkComplexity();
  }

  void checkComplexity() {
    final count = countPasswordCheck();

    if (count > 3) {
      complexityMeter = PasswordStrength.strong.enumToString();
      complexityMeterColor = Colors.lightGreenAccent;
    } else if (count > 2) {
      complexityMeter = PasswordStrength.medium.enumToString();
      complexityMeterColor = Colors.yellow;
    } else if (count > 0) {
      complexityMeter = PasswordStrength.weak.enumToString();
      complexityMeterColor = Colors.orange;
    } else {
      complexityMeter = '';
      complexityMeterColor = Colors.white;
    }
  }

  int countPasswordCheck() {
    int count = 0;
    if (isLowerCase) count++;
    if (isUpperCase) count++;
    if (isNumeric) count++;
    if (is9Chars) count++;

    return count;
  }
}
