import 'package:flutter/material.dart';
import 'package:jago_test/globals/globals.dart';
import 'package:jago_test/register/register_controller.dart';
import 'package:jago_test/register/register_screen3.dart';
import 'package:jago_test/widgets/progress_bar.dart';

class RegisterScreen2 extends StatefulWidget {
  final RegisterPageController bloc;
  RegisterScreen2({required this.bloc});

  @override
  _RegisterScreen2State createState() => _RegisterScreen2State();
}

class _RegisterScreen2State extends State<RegisterScreen2> {
  @override
  void initState() {
    super.initState();
    widget.bloc.tecPassword.addListener(() {
      setState(() => widget.bloc.checkPassword(widget.bloc.tecPassword.text));
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.bloc.tecPassword.removeListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Create Account',
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: primaryBgColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Flexible(
                child: ListView(
              children: [
                Hero(
                  tag: 'ProgressBar',
                  child: ProgressBar(progressNum: 1),
                ),
                _content(),
              ],
            )),
            _nextButton(context),
          ],
        ),
      ),
    );
  }

  Widget _content() {
    final showPassword = widget.bloc.showPassword;
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Text(
                'Create Password',
                style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.w700),
              )),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: Text(
              'Password will be used for login to account',
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 18, color: Colors.white),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            margin: EdgeInsets.only(top: 30, bottom: 30),
            padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
            child: Container(
              child: TextFormField(
                controller: widget.bloc.tecPassword,
                obscureText: !showPassword,
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    icon: Icon(!showPassword ? Icons.visibility : Icons.visibility_off),
                    onPressed: () {
                      setState(() => widget.bloc.toggleShowPassword());
                    },
                  ),
                  hintText: 'Create Password',
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          _complexity(),
        ],
      ),
    );
  }

  Widget _complexity() {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Complexity: ',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              SizedBox(width: 10),
              Text(
                widget.bloc.complexityMeter,
                style: TextStyle(color: widget.bloc.complexityMeterColor, fontSize: 20, fontWeight: FontWeight.w500),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconWithLabelWidget(
                  icon: 'a',
                  label: 'Lowercase',
                  status: widget.bloc.isLowerCase,
                ),
                IconWithLabelWidget(
                  icon: 'A',
                  label: 'Uppercase',
                  status: widget.bloc.isUpperCase,
                ),
                IconWithLabelWidget(
                  icon: '123',
                  label: 'Number',
                  status: widget.bloc.isNumeric,
                ),
                IconWithLabelWidget(
                  icon: '9+',
                  label: 'Characters',
                  status: widget.bloc.is9Chars,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _nextButton(BuildContext ctx) {
    return Container(
      height: 70,
      color: Colors.black12,
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: ElevatedButton(
        onPressed: widget.bloc.countPasswordCheck() > 3
            ? () {
                Navigator.push(
                  ctx,
                  MaterialPageRoute(builder: (ctx) => RegisterScreen3(bloc: widget.bloc)),
                );
              }
            : null,
        child: Text('Next'),
      ),
    );
  }
}

class IconWithLabelWidget extends StatelessWidget {
  final String icon;
  final String label;
  final bool status;

  IconWithLabelWidget({
    required this.icon,
    required this.label,
    required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Center(
                child: status
                    ? Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.check_circle,
                          color: Colors.green,
                          size: 60,
                        ),
                      )
                    : Text(
                        '$icon',
                        style: TextStyle(color: Colors.white, fontSize: 50, fontWeight: FontWeight.w500),
                      ),
              ),
              SizedBox(height: 10),
              Text(
                '$label',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
